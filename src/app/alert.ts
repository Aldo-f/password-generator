export class MyAlert {
    public title: string;
    public message: string;
    public okButtonText: string;
    public cancelable: boolean;

    constructor(title: string, message: string, okButtonText: string, cancelable) {
        this.title = title;
        this.message = message;
        this.okButtonText = okButtonText;
        this.cancelable = cancelable;
    }

    public set alert(v: MyAlert) {
        this.alert = v;
    }

    public get alert(): MyAlert {
        return this.alert;
    }

}
