export class PasswordCheckboxGroup {

    // export class CheckboxGroup {
    public includeLowercase: boolean;
    public includeUppercase: boolean;
    public includeSymbols: boolean;
    public includeNumbers: boolean;
    // public checkboxGroup: object = { includeLowercase, includeUppercase, includeSymbols, includeNumbers };

    constructor(includeLowercase: boolean, includeUppercase: boolean, includeSymbols: boolean, includeNumbers: boolean) {
        this.includeLowercase = includeLowercase;
        this.includeUppercase = includeUppercase;
        this.includeSymbols = includeSymbols;
        this.includeNumbers = includeNumbers;
    }
    // }
}
