export class PasswordData {
    public length: number;
    public excludeSimilar: boolean;
    public excludeAmbiguous: boolean;

    constructor(length: number, excludeSimilar: boolean, excludeAmbiguous: boolean) {
        this.length = length;
        this.excludeSimilar = excludeSimilar;
        this.excludeAmbiguous = excludeAmbiguous;
    }

    public set passwordData(v: PasswordData) {
        this.passwordData = v;
    }

    public get passwordData(): PasswordData {
        return this.passwordData;
    }
}

