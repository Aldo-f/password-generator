import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { routes } from '@src/app/app.routes';

// const routes: Routes = [
//   { path: '', pathMatch: 'full', component: GeneratorComponent }
// ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
