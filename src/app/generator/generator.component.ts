import * as application from "tns-core-modules/application";

import { AlertOptions, alert } from 'tns-core-modules/ui/dialogs';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { MyAlert } from '@src/app/alert';
import { PasswordCheckboxGroup } from '@src/app/password-checkbox-group';
import { PasswordData } from '@src/app/password-data';
import { requireCheckboxesToBeCheckedValidator } from '@src/app/generator/require-one-checkbox';

@Component({
  selector: 'app-generator',
  templateUrl: './generator.component.html',
  styleUrls: ['./generator.component.scss']
})
export class GeneratorComponent implements OnInit {
  text;
  generatedPassword;
  passwordForm;
  checkboxGroup: PasswordCheckboxGroup =
    new PasswordCheckboxGroup(true, false, false, false);
  passwordData: PasswordData =
    new PasswordData(12, true, true);

  allPasswordData = { ...this.passwordData, ...this.checkboxGroup };

  validation = {
    length: { min: 4, max: 512 }
  };


  // Metadata for Nativescript
  passwordMetadata = {
    isReadOnly: false,
    commitMode: 'Immediate',
    validationMode: 'Immediate',
    propertyAnnotations: [{
      name: 'length', displayName: 'Length',
      validators: [
        { name: 'NonEmpty' },
        {
          name: 'RangeValidator',
          params: {
            minimum: this.validation.length.min,
            maximum: this.validation.length.max,
            errorMessage: `Value must be in the range of ${this.validation.length.min} and ${this.validation.length.max}`
          }
        }]
    },
    { name: 'includeLowercase', displayName: 'Include lowercase' },
    { name: 'includeUppercase', displayName: 'Include uppercase' },
    { name: 'includeSymbols', displayName: 'Include symbols' },
    { name: 'includeNumbers', displayName: 'Include numbers' },
    { name: 'excludeSimilar', displayName: 'Exclude similar', index: 1 },
    { name: 'excludeAmbiguous', displayName: 'Exclude ambiguous', index: 1 }
    ]
  };

  constructor(private formBuilder: FormBuilder) {
    // create form
    this.passwordForm = this.formBuilder.group({
      length: new FormControl(this.allPasswordData.length, [
        Validators.required,
        Validators.min(this.validation.length.min),
        Validators.max(this.validation.length.max)
      ]),
      checkboxGroup: new FormGroup({
        includeLowercase: new FormControl(this.allPasswordData.includeLowercase),
        includeUppercase: new FormControl(this.allPasswordData.includeUppercase),
        includeSymbols: new FormControl(this.allPasswordData.includeSymbols),
        includeNumbers: new FormControl(this.allPasswordData.includeNumbers)
      }, requireCheckboxesToBeCheckedValidator()),
      excludeSimilar: this.allPasswordData.excludeSimilar,
      excludeAmbiguous: this.allPasswordData.excludeAmbiguous
    });
  }

  onSubmit(submitPasswordData) {
    console.warn(submitPasswordData);
    // fix
    submitPasswordData = this.fixSubmitData(submitPasswordData);
    this.makePassword(submitPasswordData);
    // this.passwordForm.reset();
  }


  ngOnInit() {
    this.makePassword(this.allPasswordData);
    this.text = 'Lorem ipsum';
  }

  makePassword(passwordData) {
    const length = passwordData.length;
    let result = '';
    const characters = this.generateCharacters(passwordData);

    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(
        Math.floor(Math.random() * charactersLength)
      );
    }
    this.generatedPassword = result;
  }

  remove(str, chars) {
    return str.replace(new RegExp(`[${chars}]`, 'g'), '');
  }

  generateCharacters(allPasswordData) {
    let characters = '';

    if (!allPasswordData.includeLowercase &&
      !allPasswordData.includeUppercase &&
      !allPasswordData.includeSymbols &&
      !allPasswordData.includeNumbers) {
      console.warn('No characters selected. If you don\'t choose any characters, how should I generate a password...? 🤔');
    }

    // include
    if (allPasswordData.includeLowercase || allPasswordData.includeUppercase) {
      const alphabet = 'abcdefghijklmnopqrstuvwxyz';

      if (allPasswordData.includeLowercase) {
        characters += alphabet;
      }
      if (allPasswordData.includeUppercase) {
        characters += alphabet.toUpperCase();
      }
    }
    if (allPasswordData.includeSymbols) {
      characters += '!\'#$%&\"()*+,-./:;<=>?@[\]^_`{|}~';
    }
    if (allPasswordData.includeNumbers) {
      characters += '0123456789';
    }

    // exclude
    if (allPasswordData.excludeSimilar) {
      characters = this.remove(characters, 'il1Lo0O');
    }
    if (allPasswordData.excludeAmbiguous) {
      characters = this.remove(characters, '{}}()/\'"`~,;:.<>\[\\]');
    }

    return characters;
  }

  fixSubmitData(submitPasswordData) {
    const result = { ...submitPasswordData, ...submitPasswordData.checkboxGroup };
    delete result.checkboxGroup;
    return result;
  }
}
