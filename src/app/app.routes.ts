import { GeneratorComponent } from '@src/app/generator/generator.component';
import { Routes } from '@angular/router';

export const routes: Routes = [
    { path: '', component: GeneratorComponent },
];
